# Copyright 2013 Thatcher Peskens
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM ubuntu:18.04

MAINTAINER Dockerfiles

# Install required packages and remove the apt packages cache when done.

RUN apt-get update && \
    apt-get upgrade -y && \ 
    apt-get install -y \
	git \
	python \
	python-dev \
	python-setuptools \
	python-pip \
	nginx \
	supervisor \
	redis-server \
	sqlite3 && \
	pip install -U pip setuptools && \
  	rm -rf /var/lib/apt/lists/*

# install uwsgi now because it takes a little while
RUN pip install uwsgi

# setup all the configfiles
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
COPY nginx-app.conf /etc/nginx/sites-available/default
COPY supervisor-app.conf /etc/supervisor/conf.d/

# COPY requirements.txt and RUN pip install BEFORE adding the rest of your code, this will cause Docker's caching mechanism
# to prevent re-installinig (all your) dependencies when you made a change a line or two in your app.

RUN git clone https://gitlab.com/push0rbp/salaryman  /home/docker/code/app
RUN pip install -r /home/docker/code/app/requirements.txt

# add (the rest of) our code
COPY .  /home/docker/code/ 
# install django, normally you would remove this step because your project would already
# be installed in the code/app/ directory

RUN useradd user -ms /bin/bash
RUN echo "supervised systemd" > /etc/redis/redis.conf

RUN mv /home/docker/code/settings_secret.py /home/docker/code/app/salaryman
RUN python /home/docker/code/app/manage.py collectstatic --noinput
RUN mv /home/docker/code/init.py /home/docker/code/app/app/management/commands
RUN python /home/docker/code/app/manage.py init 
RUN rm /home/docker/code/app/app/management/commands/init.py
RUN chown user.user /home/docker/code/app/db
RUN chown user.user /home/docker/code/app/db/db.sqlite3

COPY th1s_1s_rea1_flag /
RUN chmod 400 th1s_1s_rea1_flag
RUN chown user.user th1s_1s_rea1_flag
RUN rm /home/docker/code/th1s_1s_rea1_flag

COPY start.sh /
RUN chmod 755 /start.sh
COPY run.sh /
RUN chmod 755 /run.sh

EXPOSE 80
CMD ["bash", "/run.sh"]